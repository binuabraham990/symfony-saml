<?php

namespace AppBundle\Security;

use AppBundle\Entity\User;
use Hslavich\OneloginSamlBundle\Security\Authentication\Token\SamlTokenInterface;
use Hslavich\OneloginSamlBundle\Security\User\SamlUserFactoryInterface;

class UserFactory implements SamlUserFactoryInterface {

    public function createUser(SamlTokenInterface $token) {
        $useremail = $token->getUser();
        $user = new User();
        $user->setRoles(array('ROLE_USER'));
        $user->setUsername($useremail);
        $user->setPassword('notused');
        $user->setEmail($useremail);
        $user->setEmailCanonical($useremail);

        return $user;
    }

}
